package com.thoughtworks.vapasi.quantity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Measurement {
    private double value;
    private Unit unit;


    private Measurement(double value, Unit unit) {
        this.value = value;
        this.unit = unit;
    }

    public static Measurement kilometer(double value) {
        Measurement measurement = new Measurement(value, new Unit(QuantityConstants.KILOMETER, QuantityConstants.LENGTH));
        return measurement;
    }

    public static Measurement meter(double value) {
        Measurement measurement = new Measurement(value, new Unit(QuantityConstants.METER, QuantityConstants.LENGTH));
        return measurement;
    }

    public static Measurement centimeter(double value) {
        Measurement measurement = new Measurement(value, new Unit(QuantityConstants.CENTIMETER, QuantityConstants.LENGTH));
        return measurement;
    }

    public static Measurement grams(double value) {
        Measurement measurement = new Measurement(value, new Unit(QuantityConstants.GRAM, QuantityConstants.WEIGHT));
        return measurement;
    }

    public static Measurement kiloGrams(double value) {
        Measurement measurement = new Measurement(value, new Unit(QuantityConstants.KILOGRAM, QuantityConstants.WEIGHT));
        return measurement;
    }

    @Override
    public boolean equals(Object o) {
        if(! unit.checkIfTypeIsSame(this.unit, ((Measurement) o).unit)){
            return false;
        }
        return Double.compare((this.value) * (unit.convertToBaseValue(this.unit)),
                (((Measurement) o).value) * (unit.convertToBaseValue(((Measurement) o).unit))) == 0;
    }

    public Measurement add(Measurement other) {

        if(! unit.checkIfTypeIsSame(this.unit, ((Measurement) other).unit)){
            throw new IllegalArgumentException();
        }
        return new Measurement((((other.unit.convertToBaseValue(this.unit) * this.value)+
                ((other.unit.convertToBaseValue(other.unit) * other.value)))/unit.convertToBaseValue(this.unit)),
                this.unit);
    }

    public Measurement subtract(Measurement other) {
        if(! unit.checkIfTypeIsSame(this.unit, ((Measurement) other).unit)){
            throw new IllegalArgumentException();
        }
        return new Measurement((((other.unit.convertToBaseValue(this.unit) * this.value) -
                ((other.unit.convertToBaseValue(other.unit) * other.value)))/unit.convertToBaseValue(this.unit)),
                this.unit);
    }

    @Override
    public String toString() {
        return value +" "+ unit;
    }
}
