package com.thoughtworks.vapasi.quantity.test;

import com.thoughtworks.vapasi.quantity.Unit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UnitTest {
    Unit unit;

    @BeforeEach
    void setUp(){
        unit = new Unit();
    }

    @Test
    void shouldCheckIfTypeIsSame() {
        assertTrue(unit.checkIfTypeIsSame(new Unit("meter", "length"), new Unit("kilometer", "length")));
    }

    @Test
    void shouldCheckIfTypeIsNotSame() {
        assertFalse(unit.checkIfTypeIsSame(new Unit("gram", "weight"), new Unit("kilometer", "length")));
    }

    @Test
    void shouldCheckForEqualityOfConversionToBaseValue() {
        assertEquals(unit.convertToBaseValue(new Unit("meter", "length")
        ), unit.convertToBaseValue(new Unit("meter", "length")));
    }

    @Test
    void shouldCheckIfConversionToBaseValueIsNotEqual() {
        assertNotEquals(unit.convertToBaseValue(new Unit("meter", "length")
        ), unit.convertToBaseValue(new Unit("kilometer", "length")));
    }

}
