package com.thoughtworks.vapasi.quantity.test;

import com.thoughtworks.vapasi.quantity.Measurement;
import com.thoughtworks.vapasi.quantity.Unit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MeasurementTest {
    @Test
    void shouldCheckForEqualityOfMetersAndCentimetersMeasurements() {
        assertEquals(Measurement.meter(1), Measurement.centimeter(100));
    }

    @Test
    void shouldCheckIfMetersAndCentimetersMeasurementsAreNotEqual() {
        assertNotEquals(Measurement.meter(2), Measurement.centimeter(300));
    }

    @Test
    void shouldCheckForEqualityOfMetersAndKiloMetersMeasurements() {
        assertEquals(Measurement.meter(1000), Measurement.kilometer(1));
    }

    @Test
    void shouldCheckIfMetersAndKilometerMeasurementsAreNotEqual() {
        assertNotEquals(Measurement.meter(1000), Measurement.kilometer(2));
    }

    @Test
    void shouldCheckForEqualityOfGramsAndKiloGramsMeasurements() {
        assertEquals(Measurement.grams(1000), Measurement.kiloGrams(1));
    }

    @Test
    void shouldCheckIfGramsAndKiloGramsMeasurementsAreNotEqual() {
        assertNotEquals(Measurement.grams(2000), Measurement.kiloGrams(1));
    }

    @Test
    void shouldCheckIfUnitTypesAreCompatible(){
        assertNotEquals(Measurement.centimeter(100),Measurement.grams(300));
    }

    @Test
    void shouldAddKilometerAndMeter(){
        assertEquals("1.1 kilometer",Measurement.kilometer(1).add(Measurement.meter(100)).toString());
    }

    @Test
    void shouldThrowExceptionIfTwoIncompatibleQuantitiesAreAdded() {
        assertThrows(IllegalArgumentException.class, ()->Measurement.grams(200).add(Measurement.meter(1)).getClass());
    }
    @Test
    void shouldSubtractKilometerAndMeter(){
        assertEquals("0.9 kilometer",Measurement.kilometer(1).subtract(Measurement.meter(100)).toString());
    }
    @Test
    void shouldThrowExceptionIfTwoIncompatibleQuantitiesAreSubtracted() {
        assertThrows(IllegalArgumentException.class, ()->Measurement.grams(3000).subtract(Measurement.centimeter(100)).getClass());
    }




}
